var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var clienteSchema = new Schema({
	nombres: 		{ type: String },
	apellidos: 		{ type: String },
	email: 	{ type: String ,
		unique:true, 
		required:[true,'El correo es obligatorio']
	},
	password:  	{ type: String },
	direccion: 	{ type: String },
	createdAt:  	{ type: Date, default: Date.now },
	updateAt: 	{ type: Date },
	tipoDocumento: 	{ type: String, enum :
					['DNI', 'PASAPORTE']
				},
	numeroDocumento: 	{ type: String }    
});



module.exports = mongoose.model('Cliente', clienteSchema);