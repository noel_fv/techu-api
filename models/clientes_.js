const mongoose = require('mongoose'),
	//Schema = mongoose.Schema;

var clientesSchema =  mongoose.Schema({
	nombres: 		{ type: String },
	apellidos: 		{ type: String },
	email: 	{ type: String ,
		unique:true, 
		required:[true,'El correo es obligatorio']
	},
	password:  	{ type: String },
	direccion: 	{ type: String },
	createdAt:  	{ type: Date, default: Date.now },
	updateAt: 	{ type: Date },
	tipoDocumento: 	{ type: String, enum :
					['DNI', 'PASAPORTE']
				},
	numeroDocumento: 	{ type: String }    
});



module.exports = mongoose.model('Clientes', clientesSchema);