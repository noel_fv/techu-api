//File: routes/tvshows.js
module.exports = function(app) {

  var Clientes = require('../models/cliente.js/index.js');




  //GET - Return all tvshows in the DB
  findAllClientes = function(req, res) {
	Clientes.find(function(err, clientes) {
  		if(!err) {
        console.log('GET /clientes')
  			res.send(clientes);
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});
  };

  //GET - Return a TVShow with specified ID
  findById = function(req, res) {
	Clientes.findById(req.params.id, function(err, clientes) {
  		if(!err) {
        console.log('GET /clientes/' + req.params.id);
  			res.send(clientes);
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});
  };

  //POST - Insert a new TVShow in the DB
  addClientes = function(req, res) {
  	console.log('POST');
  	console.log(req.body);

  	var cliente = new Clientes({
		nombres:    req.body.nombres,
		apellidos: 	  req.body.apellidos,
		email:  req.body.email,
		password:   req.body.password,
		direccion:  req.body.direccion,
		tipoDocumento:    req.body.tipoDocumento,
		numeroDocumento:  req.body.numeroDocumento  
	  });
	    

  	Clientes.save(function(err) {
  		if(!err) {
  			console.log('Created');
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});

  	res.send(cliente);
  };

  

  //Link routes and functions
  app.get('/clientes', findAllClientes);
  app.get('/clientes/:id', findById);
  app.post('/clientes', addClientes);
 // app.put('/tvshow/:id', updateTVShow);
  //app.delete('/tvshow/:id', deleteTVShow);

}