//File: routes/tvshows.js
module.exports = function(app) {

  var Cliente = require('../models/cliente.js');

  //GET - Return all tvshows in the DB
  findAllTVShows = function(req, res) {
	Cliente.find(function(err, tvshows) {
  		if(!err) {
        console.log('GET /tvshows')
  			res.send(tvshows);
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});
  };

  //GET - Return a TVShow with specified ID
  findById = function(req, res) {
	Cliente.findById(req.params.id, function(err, tvshow) {
  		if(!err) {
        console.log('GET /tvshow/' + req.params.id);
  			res.send(tvshow);
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});
  };

  //POST - Insert a new TVShow in the DB
  addTVShow = function(req, res) {
  	console.log('POST');
  	console.log(req.body);

  	var cliente = new Cliente({
		nombres:    req.body.nombres,
		apellidos: 	  req.body.apellidos,
		email:  req.body.email,
		password:   req.body.password,
		direccion:  req.body.direccion,
		tipoDocumento:    req.body.tipoDocumento,
		numeroDocumento:  req.body.numeroDocumento  
  	});

  	cliente.save(function(err) {
  		if(!err) {
  			console.log('Created');
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});

  	res.send(cliente);
  };

  //PUT - Update a register already exists
  updateTVShow = function(req, res) {
	Cliente.findById(req.params.id, function(err, cliente) {
		  cliente.nombres=    req.body.nombres,
		  cliente.apellidos= 	  req.body.apellidos,
		  cliente.email=  req.body.email,
		  cliente.password=   req.body.password,
		  cliente.direccion=  req.body.direccion,
		  cliente.tipoDocumento=    req.body.tipoDocumento,
		  cliente.numeroDocumento=  req.body.numeroDocumento  

  		cliente.save(function(err) {
  			if(!err) {
  				console.log('Updated');
  			} else {
  				console.log('ERROR: ' + err);
  			}
  			res.send(cliente);
  		});
  	});
  }

  //DELETE - Delete a TVShow with specified ID
  deleteTVShow = function(req, res) {
  	TVShow.findById(req.params.id, function(err, tvshow) {
  		tvshow.remove(function(err) {
  			if(!err) {
  				console.log('Removed');
  			} else {
  				console.log('ERROR: ' + err);
  			}
  		})
  	});
  }

  //Link routes and functions
  app.get('/clientes', findAllTVShows);
  app.get('/cliente/:id', findById);
  app.post('/cliente', addTVShow);
  app.put('/cliente/:id', updateTVShow);
  app.delete('/cliente/:id', deleteTVShow);

}