var express  = require("express"),
    app      = express(),
    http     = require("http"),
    server   = http.createServer(app),
    mongoose = require('mongoose'); 

app.configure(function () {
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});

app.get('/', function(req, res) {
  res.send("Hello world!");
});

routes = require('./routes/tvshows')(app);



const uri = 'mongodb+srv://test:test@cluster0-h99at.mongodb.net/test?retryWrites=true';
/*
const MongoClient = require('mongodb').MongoClient;
const clientDB = new MongoClient(uri, { useNewUrlParser: true });
clientDB.connect(err => {
  if(err) {
		console.log('ERROR: connecting to Database. ' + err);
	} else {
		console.log('Connected to Database');
	}
});
*/

/*
mongoose.connect('mongodb://localhost/tvshows', function(err, res) {
	if(err) {
		console.log('ERROR: connecting to Database. ' + err);
	} else {
		console.log('Connected to Database');
	}
});

var options = {
  useMongoClient: true,
  autoIndex: false, // Don't build indexes
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  //dbName: 'test'
};

mongoose.connect('mongodb+srv://test:test@cluster0-h99at.mongodb.net/test?retryWrites=true', options,function(err) {
  if(err) {
		console.log('ERROR: connecting to Database. ' + err);
	} else {
		console.log('Connected to Database');
	}
});

"mongoose": "3.6.11",
*/

mongoose.connect(uri, { dbName: 'techudb'},function(err) {
  if(err) {
		console.log('ERROR: connecting to Database. ' + err);
	} else {
    console.log('Connected to Database');
/*
Schema = mongoose.Schema;
var clientesSchema = new Schema({
	nombres: 		{ type: String },
	apellidos: 		{ type: String },
	email: 	{ type: String },
	password:  	{ type: String },
	direccion: 	{ type: String },
	createdAt:  	{ type: Date, default: Date.now },
	updateAt: 	{ type: Date },
	tipoDocumento: 	{ type: String, enum :
					['DNI', 'PASAPORTE']
				},
	numeroDocumento: 	{ type: String }    
});

    var ClientesDao = mongoose.model('Clientes', clientesSchema);
    // Works
    ClientesDao.find(function(err, clientes) {
  		if(!err) {
        console.log('GET /clientes'+clientes)
  		//	res.send(clientes);
  		} else {
  			console.log('ERROR: ' + err);
  		}
    });
    */
	}
});

server.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});