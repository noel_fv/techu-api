var express  = require("express"),
    app      = express(),
    http     = require("http"),
    server   = http.createServer(app),
    mongoose = require('mongoose'); 

app.configure(function () {
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});

app.get('/', function(req, res) {
  res.send("Hello world!");
});

routes = require('./routes/tvshows')(app);
routes = require('./routes/clientes')(app);

let uri = 'mongodb+srv://test:test@cluster0-h99at.mongodb.net/test?retryWrites=true';

mongoose.connect(uri, { dbName: 'techudb'},function(err) {
  if(err) {
		console.log('ERROR: connecting to Database. ' + err);
	} else {
    console.log('Connected to Database');
/*
Schema = mongoose.Schema;
var clientesSchema = new Schema({
	nombres: 		{ type: String },
	apellidos: 		{ type: String },
	email: 	{ type: String },
	password:  	{ type: String },
	direccion: 	{ type: String },
	createdAt:  	{ type: Date, default: Date.now },
	updateAt: 	{ type: Date },
	tipoDocumento: 	{ type: String, enum :
					['DNI', 'PASAPORTE']
				},
	numeroDocumento: 	{ type: String }    
});

    var ClientesDao = mongoose.model('Clientes', clientesSchema);
    // Works
    ClientesDao.find(function(err, clientes) {
  		if(!err) {
        console.log('GET /clientes'+clientes)
  		//	res.send(clientes);
  		} else {
  			console.log('ERROR: ' + err);
  		}
    });
    */
	}
});

server.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});